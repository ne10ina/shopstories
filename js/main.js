var len = 0 // Количество карточек
var mySwiper = 0; // Объект свайпера
var firstRun = true; // Первый ли запуск

// Первый раз иницализируем свайпер
initSwiper();
sortSlides();

// Добавляем действие на нажатие кнопки Выбрать еще
let buttonsShowMore = $('.select-more').on('click', showFeed);

// Устанавливаем высоту карточки товаров
let height = $('.saved').height();
$('.swiper-container').css('height', 'calc(100% - ' + height.toString() + 'px)');

// Нажатие кнопки удалить на странице сохраненных товаров
$(document).on('click', '.swiper-slide-meta-buttons__saved__remove', function () {
    let parent = $(this).closest('.swiper-slide-meta-buttons');
    let slide = $(this).closest('.swiper-slide');
    let id = $(slide).attr('data-id');

    // Убираем класс для избранного и добавляем класс для нравится
    parent.removeClass('sDislike').removeClass('sLike').addClass('sSaved');

    // Убираем классы и обновляем состояние для всех карточек, у который этот id
    $('.swiper-slide[data-id=' + id + ']').attr('data-wishlist', 'no_wishlist');
    $('.swiper-slide[data-id=' + id + '] .swiper-slide-meta-buttons').removeClass('sSaved').removeClass('sDislike').addClass('sLike');

    // Сохраняем обновленное состояние и убираем карточку из избранных
    updateWishState($(this).closest('.swiper-slide').attr('data-id'), 'no_wishlist');
    slide.remove();
});

// Нажатие кнопки заказать
$(document).on('click', '.swiper-slide-meta-buttons__saved__order', function () {
    let parent = $(this).closest('.swiper-slide-meta-buttons');
    let slide = $(this).closest('.swiper-slide');
    let id = $(slide).attr('data-id');

    // Графически показываем, что сделали заказ
    slide.find('.swiper-slide-meta-buttons__saved__order > .button').addClass('order');

    // Обновляем состояние заказа и сохраняем его
    slide.attr('data-order', 'have_order');
    $('.swiper-slide[data-id=' + id + ']').attr('data-order', 'no_order');
    updateOrderState(id, 'have_order');
});

// Инициализация свайпера
function initSwiper() {
    let buttonsNext = $('.swiper-slide-meta-buttons__next').on('click', () => { })

    var mySwiper = new Swiper('.swiper-container', {
        effect: 'cube',
        speed: 700,
        preventInteractionOnTransition: true,
        cubeEffect: {
            shadow: true,
            slideShadows: true,
            shadowOffset: 20,
            shadowScale: 0.94,
        },
    });

    mySwiper.slideTo(1, 0);

    mySwiper.on('transitionStart', () => {
        $('.swiper-slide-clone').remove();
    });


    mySwiper.on('transitionEnd', () => {
        if (mySwiper.activeIndex == window.len - 1) {
            mySwiper.slideTo(1, 0);
        }
        else if (mySwiper.activeIndex == 0) {
            mySwiper.slideTo(window.len - 2, 0);
        }
        currentSlide();
    });

    mySwiper.on('slideChange', () => {
        $(mySwiper.slides[mySwiper.activeIndex]).attr('data-view', 'have_view');
        updateViewState($(mySwiper.slides[mySwiper.activeIndex]).attr('data-id'), 'have_view');
    });

    window.mySwiper = mySwiper;
}

function currentSlide() {
    let slide = $(mySwiper.slides[mySwiper.activeIndex]).clone();
    let parent = $('.swiper-container');
    parent.remove('swiper-slide-clone');
    parent.prepend(slide);
    slide.addClass('swiper-slide-clone').css('z-index', '10').css('position', 'absolute').css('opacity', '0');
    slide.find('.swiper-slide-meta-buttons').css('z-index', '10');
    slide.find('.swiper-slide-meta-buttons-action__like__add').on('click', () => {
        let id = $('.swiper-slide-clone').attr('data-id');
        let slide = $('.swiper-slide[data-id=' + id + ']');
        let parent = slide.find('.swiper-slide-meta-buttons');
        parent.removeClass('sLike').removeClass('sSaved').addClass('sDislike');
        slide.attr('data-wishlist', 'have_wishlist');
        updateWishState(id, 'have_wishlist');
        mySwiper.slideNext();
    });

    slide.find('.swiper-slide-meta-buttons-action__dislike__remove').on('click', () => {
        let id = $('.swiper-slide-clone').attr('data-id');
        let slide = $('.swiper-slide[data-id=' + id + ']');
        let parent = slide.find('.swiper-slide-meta-buttons');
        parent.removeClass('sDislike').removeClass('sSaved').addClass('sLike');
        slide.attr('data-wishlist', 'no_wishlist');
        updateWishState($(this).closest('.swiper-slide').attr('data-id'), 'no_wishlist');
    });

    slide.find('.swiper-slide__img__left').on('click', () => {
        mySwiper.slidePrev();
    });
    slide.find('.swiper-slide__img__right').on('click', () => {
        mySwiper.slideNext();
    });
    slide.find('.swiper-slide-meta-buttons__next').on('click', () => {
        mySwiper.slideNext();
    });
    $('.savedClick').off('click').on('click', showItems);
}

// Показ сохраненных товаров
function showItems() {
    // Добавляем классы для правильного отображения страницы с сохареннными товарами, скрываем ленту товаров
    $('.container').addClass('container-saved');
    $('.select-wrapper').toggleClass('hide');
    $('body').toggleClass('bodyy');

    // После проигрывания анимации, показываем сохранненные товары
    setTimeout(function () {
        // Добавляем классы для отображения страницы сохраенных товаров
        $('.select-wrapper').toggleClass('dnone');
        $('.saved-wrapper').toggleClass('dblock');
        $('.saved-wrapper').toggleClass('show');

        // Собираем сохраненные товары
        collectWishlist();
        $('.swiper-slide').remove('.swiper-slide-clone');
    }, 200);
    $('.swiper-slide').remove('.swiper-slide-clone');
}

// Показ ленты товаров
function showFeed() {
    // Добавляем классы для правильонго отображения ленты товаров, скрываем сохраненные товары
    $('.container').removeClass('container-saved');
    $('.saved-wrapper').toggleClass('show');
    $('body').toggleClass('bodyy');

    // После проигрывания анимции, показываем ленту товаров
    setTimeout(function () {
        // Добавляем классы для отображения ленты товаров
        $('.select-wrapper').toggleClass('dnone');
        $('.select-wrapper').toggleClass('hide');
        $('.saved-wrapper').toggleClass('dblock');

        // Сортируем слайды по просмотрам
        sortSlides();
    }, 200);
}

// Сбор избранных карточек и показ их на странице
function collectWishlist() {
    let parent = $('.saved-items');

    // Убираем дубликаты слайдов и уничтожаем свайпер
    $('.swiper-slide').remove('.swiper-slide[data-type="dublicate"]').remove('.swiper-slide-clone');
    mySwiper.destroy(false, true);

    // Очищаем контейнер от предыдущих карточек и добавляем туда новые
    parent.empty();
    parent.append($(document).find('.swiper-slide[data-wishlist="have_wishlist"]').clone());

    // Для карточек с have_order выводим состояние
    parent.find('.swiper-slide[data-order="have_order"]').each(function (i, el) {
        $(el).find('.swiper-slide-meta-buttons__saved__order > .button').addClass('order');
    });

    // Добавляем для них картинки и убираем background-image
    parent.find('.swiper-slide').each(function (i) {
        $(this).find('.swiper-slide__img').css('background-image', 'unset').append('<img src="' + $(this).attr('data-imgsrc') + '">');
    });
}

// Функция для восстановления состояния товаров
function firstRunFire() {
    restoreState();
}

// Сортировка слайдов
function sortSlides() {
    // Если это первый запуск, то нужно восстановить состояния карточек
    firstRunFire();

    // Уничтожаем свайпер
    mySwiper.destroy(false, true);

    // Убираем дубликаты слайдов
    $('.swiper-slide').remove("[data-type='dublicate']");

    var container = $('.swiper-wrapper');
    var slides = $('.swiper-wrapper > .swiper-slide').detach();

    $('.swiper-container').remove('swiper-slide-clone');

    // Сортируем слайды по просмотру, если просмотры одинаковы, то по id
    slides = slides.sort(function (a, b) {
        var aView = $(a).attr('data-view') == "no_view" ? 0 : 1;
        var bView = $(b).attr('data-view') == "no_view" ? 0 : 1;
        var aId = parseInt($(a).attr('data-id'));
        var bId = parseInt($(b).attr('data-id'));

        if (aView > bView)
            return 1;
        else if (aView < bView)
            return -1;
        else if (aView == bView) {
            if (aId > bId)
                return 1;
            return -1;
        }
    });

    // Отображаем слайды на сайте
    container.html(slides);

    // Добавляем дубликаты в концы для бесконечного отображения
    container.append($(container.children()[0]).clone());
    container.prepend($(container.children()[container.children().length - 2]).clone());
    $(container.children()[0]).attr('data-type', 'dublicate');
    $(container.children()[container.children().length - 1]).attr('data-type', 'dublicate');

    // Обновляем состояние просмотра для нового первого слайда
    updateViewState($(container.children()[1]).attr('data-id'), 'have_view')
    $(container.children()[1]).attr('data-view', 'have_view');

    // Сохраняем количество слайдов и инициализируем свайпер и вставляем картинки
    window.len = container.children().length;
    initSwiper();
    imgToBgImg();
    currentSlide();
    firstRun = false;
};

// Вставка картинок из data-imgsrc
function imgToBgImg() {
    $('.swiper-slide').each(function (i) {
        $(this).find('.swiper-slide__img').css('background-image', "url('" + $(this).attr('data-imgsrc') + "')")
    });
}

// СОСТОЯНИЕ ТОВАРОВ
// Состояние архитектура ↓
// [id, view, wishlist, order]

// Проверка, существет ли состояние карточки. Если нет, то создаем его
function checkCookieExist(key) {
    console.log('Check cookie exist with key = ' + key + '; Cookie = ' + Cookies.get(key));
    if (Cookies.get(key) == null)
        createCookie(key);
}

// Получение данных из сохраненного состояния
function getArrayFromCookieState(id) {
    checkCookieExist('id' + id);
    var obj = Cookies.get('id' + id);
    var data = obj.replace('[', '').replace(']', '').replace('"', '').replace("'", '').split(',');
    return data;
}

// Обновление состояния просмотра
function updateViewState(id, state) {
    updateMaster(id, 1, state);
    console.log('Update view state with id = ' + id + ' and data = ' + getArrayFromCookieState(id) + ' and state = ' + state);
}

// Обновление состояние избранного
function updateWishState(id, state) {
    updateMaster(id, 2, state);
    console.log('Update wish state with id = ' + id + ' and data = ' + getArrayFromCookieState(id) + ' and state = ' + state);
}

// Обновить состояние покупки
function updateOrderState(id, state) {
    updateMaster(id, 3, state);
    console.log('Update order state with id = ' + id + ' and data = ' + getArrayFromCookieState(id) + ' and state = ' + state);
}

// Обновление состояния по id, index состояния для обновления
function updateMaster(id, index, state) {
    var data = getArrayFromCookieState(id);
    data[index] = state;
    Cookies.set('id' + id, data.toString(), { expires: 365 });
}

// Создание пустого состояния
function createCookie(key) {
    a = [key.replace('i', '').replace('d', ''), 'no_view', 'no_wishlist', 'no_order'];
    Cookies.set(key, a.toString(), { expires: 365 })
    console.log('Created cookie with key = ' + key + ' and data = ' + a);
}

// Удаление всех состояний
function deleteCookies() {
    var obj = Cookies.get();
    Object.keys(obj).forEach(key => {
        Cookies.remove(key);
    });
}

// Восстановление состояния из сохраненных состояний
function restoreState() {
    console.log('Cookies to restore = ');
    console.log(Cookies.get());
    var obj = Cookies.get();
    // Если нет сохраненных значений return
    if (obj == null)
        return;
    Object.keys(obj).forEach(key => {
        if(key.startsWith('id'))
        {
            // Восстанавливаем и присваиваем сохраненные значения
            var data = obj[key].replace('[', '').replace(']', '').replace('"', '').replace("'", '').split(',');
            console.log('Data to set = ' + data);
            $('.swiper-slide[data-id=' + data[0] + ']').attr('data-view', data[1]).attr('data-wishlist', data[2]).attr('data-order', data[3]);

            // Добавляем классы если карточка сохранена
            if (data[2] == 'have_wishlist')
                $('.swiper-slide[data-id=' + data[0] + ']').find('.swiper-slide-meta-buttons').removeClass('sLike').removeClass('sSaved').addClass('sDislike');
            else
                $('.swiper-slide[data-id=' + data[0] + ']').find('.swiper-slide-meta-buttons').removeClass('sDislike').removeClass('sSaved').addClass('sLike');
        }
    });
}